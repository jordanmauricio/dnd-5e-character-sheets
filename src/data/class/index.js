import Artificer from './class-artificer.json'
import Barbarian from './class-barbarian.json'
import Bard from './class-bard.json'
import Cleric from './class-cleric.json'
import Druid from './class-druid.json'
import Fighter from './class-fighter.json'
import Monk from './class-monk.json'
import Paladin from './class-paladin.json'
import Ranger from './class-ranger.json'
import Rogue from './class-rogue.json'
import Sorcerer from './class-sorcerer.json'
import Warlock from './class-warlock.json'
import Wizard from './class-wizard.json'

export {
  Artificer,
  Barbarian,
  Bard,
  Cleric,
  Druid,
  Fighter,
  Monk,
  Paladin,
  Ranger,
  Rogue,
  Sorcerer,
  Warlock,
  Wizard,
}
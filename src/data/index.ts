import slugify from 'slugify';

import PHBSpells from './spells-phb.json';
import XGESpells from './spells-xge.json';
import TCESpells from './spells-tce.json';
import EGWSpells from './spells-egw.json';
import Items from './items.json';
import ItemsBase from './items-base.json';
import Races from './races.json';
import * as Classes from './class';
import Backgrounds from './backgrounds.json';
import Feats from './feats.json';
import Languages from './languages.json';
import OptionalFeatures from './optionalfeatures.json';

import { createTime, createRange, createDuration, checkForSource } from '../utils';
import { Class, FormattedClass } from '../types';

export const spellBooks = [PHBSpells, XGESpells, TCESpells, EGWSpells];

export const spells = spellBooks
  .map((book) =>
    book.spell.map((spell: any) => ({
      label: spell.name,
      value: slugify(spell.name, { lower: true }),
      from: `${spell.source} #${spell.page}`,
      level: spell.level,
      school: spell.school,
      time: createTime(spell),
      range: createRange(spell),
      duration: createDuration(spell),
      body: `${spell.entries}`,
      ritual: !!spell.meta?.ritual,
      components: {
        v: !!spell.components.v,
        s: spell.components.s,
        m: spell.components.m ?? false,
      },
    })),
  )
  .flat();

const itemsStore = Items.item.map((item) => ({
  label: item.name,
  value: slugify(item.name, { lower: true }),
  from: `${item.source} #${item.page}`,
  rarity: item.rarity,
  attunement: item.reqAttune ? `Requires attunement ${item.reqAttune}` : 'No',
  isBase: false,
  entries: JSON.stringify(item.entries),
}));

const itemsBase = ItemsBase.baseitem.map((item) => ({
  label: item.name,
  value: slugify(item.name, { lower: true }),
  from: `${item.source} #${item.page}`,
  rarity: item.rarity,
  isBase: true,
  attunement: 'No',
  entries: JSON.stringify(item.entries),
}));

export const items = itemsStore.concat(itemsBase);

export const races = Races.race
  .map((race) => {
    if (checkForSource(race)) {
      // manually filter out certain sources
      // select the Triton from VGM instead
      if (race.name === 'Triton' && race.source === 'MOT') return;
      return {
        label: race.name,
        value: slugify(race.name, { lower: true }),
        from: `${race.source} #${race.page}`,
        size: race.size,
        speed: race.speed,
        srd: race.srd ?? null,
        darkvision: race.darkvision ?? null,
        resist: race.resist ?? null,
        additionalSpells: race.additionalSpells ?? null,
        subraces: race.subraces?.map((subrace: any) => {
          const name = subrace.name ? subrace.name : 'Standard';
          const source = subrace.source ? subrace.source : race.source;
          subrace.entries = JSON.stringify(subrace.entries);

          return {
            ...subrace,
            label: `${name} - ${source}`,
            value: slugify(name, { lower: true }),
            from: `${source} #${subrace.page}`,
          };
        }),
        creatureTypes: race.creatureTypes ?? null,
        traitTags: race.traitTags ?? null,
      };
    }
  })
  .filter((race, i, array) => array.findIndex((item) => item?.label === race?.label) === i)
  .filter((race) => race !== undefined);

export const backgrounds = Backgrounds.background.map((bg: any) => ({
  label: bg.name,
  value: slugify(bg.name, { lower: true, remove: /[']/g }),
  from: `${bg.source} #${bg.page}`,
}));

export const feats = Feats.feat
  .map((feat: any) => {
    if (checkForSource(feat)) {
      return {
        label: feat.name,
        value: slugify(feat.name, { lower: true, remove: /[']/g }),
        from: `${feat.source} #${feat.page}`,
      };
    }
  })
  .filter((feat, i, array) => array.findIndex((item) => item?.label === feat?.label) === i)
  .filter((feat) => feat !== undefined);

export const languages = Languages.language
  .map((lang) => {
    if (checkForSource(lang)) {
      return {
        label: lang.name,
        value: slugify(lang.name, { lower: true, remove: /[']/g }),
      };
    }
  })
  .filter((lang, i, array) => array.findIndex((item) => item?.label === lang?.label) === i)
  .filter((lang) => lang !== undefined);

export const classes: FormattedClass[] = [];
const retrievedClasses: { [k: string]: Class } = Classes;
for (const [key, value] of Object.entries(retrievedClasses)) {
  // console.log(`${key}: `, value)
  if (key !== 'Sidekick') {
    if (key === 'Artificer') {
      // filter out UA artificers
      value.class = value.class.filter((classType) => classType.source === 'TCE');
    }

    const formattedFeatures = value.classFeature.map((feature) => ({
      ...feature,
      entries: JSON.stringify(feature.entries),
    }));

    classes.push({
      name: key,
      label: `${key}`,
      value: slugify(key, { lower: true }),
      from: `${value.class[0].source} #${value.class[0].page}`,
      hd: `d${value.class[0].hd?.faces}`,
      srd: !!value.class[0].srd,
      features: formattedFeatures,
      saveProficiency: value.class[0].proficiency,
      casterProgression: value.class[0].casterProgression ? value.class[0].casterProgression : null,
      castingAbility: value.class[0].spellcastingAbility ? value.class[0].spellcastingAbility : null,
      cantrips: value.class[0].cantripProgression ? value.class[0].cantripProgression : null,
      spellsKnown: value.class[0].spellsKnownProgression ? value.class[0].spellsKnownProgression : null,
      preparesSpells: !!value.class[0].preparedSpells,
      optionalFeatureProgression: value.class[0].optionalfeatureProgression ?? null,

      subclasses: value.class[0].subclasses
        ?.map((sub) => {
          if (checkForSource(sub)) {
            const subFeats = value.subclassFeature?.filter((feat) => feat.subclassShortName === sub.shortName);
            // console.log({ subFeats })
            return {
              name: sub.shortName,
              fullName: sub.name,
              from: `${sub.source} #${sub.page}`,
              label: sub.name,
              value: sub.shortName,
              features:
                !!subFeats &&
                subFeats.map((feat) => ({
                  name: feat.name,
                  from: `${feat.source} #${feat.page}`,
                  level: feat.level,
                })),
            };
          }
        })
        .filter((sub) => sub !== undefined),
    });
  }
}

// optional features
export const invocations = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (checkForSource(optFeat) && optFeat.featureType.includes('EI')) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

export const infusions = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (checkForSource(optFeat) && optFeat.featureType.includes('AI')) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

export const arcaneShot = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (checkForSource(optFeat) && optFeat.featureType.includes('AS')) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

export const elementalDiscipline = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (checkForSource(optFeat) && optFeat.featureType.includes('ED')) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

export const metaMagic = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (checkForSource(optFeat) && optFeat.featureType.includes('MM')) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

export const runeKnightRunes = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (checkForSource(optFeat) && optFeat.featureType.includes('RN')) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

export const pactBoons = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (checkForSource(optFeat) && optFeat.featureType.includes('PB')) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

export const maneuvers = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (checkForSource(optFeat) && optFeat.featureType.includes('MV:B')) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

export const fightingStyles = OptionalFeatures.optionalfeature
  .map((optFeat) => {
    if (
      checkForSource(optFeat) &&
      (optFeat.featureType.includes('FS:R') || // ranger
        optFeat.featureType.includes('FS:P') || // paladin
        optFeat.featureType.includes('FS:F') || // fighter
        optFeat.featureType.includes('FS:B')) // bard
    ) {
      return {
        label: optFeat.name,
        value: slugify(optFeat.name, { lower: true }),
        ...optFeat,
      };
    }
  })
  .filter((item) => item !== undefined);

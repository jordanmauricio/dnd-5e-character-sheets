import firebase from '../lib/firebase';
import { Character } from '../types';

export const addUser = async (authUser: any) => {
  const resp = await firebase
    .firestore()
    .collection('users')
    .doc(authUser.uid as string)
    .set({ ...authUser }, { merge: true });
  return resp;
};

export const addCharacter = async (characterData: Character) => {
  const response = await firebase
    .firestore()
    .collection('characters')
    .doc(characterData.id)
    .set(characterData)
    .then(() => {
      console.log('Document successfully written');
    })
    .catch((error) => {
      console.error('Error adding document: ', error);
    });
  return response;
};

export const getAllCharacters = async (userId: string) => {
  if (!userId) return [];
  const snapshot = await firebase.firestore().collection('characters').where('uuid', '==', userId).get();
  const characters = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
  return characters;
};

export const getCharacter = async (characterId: string) => {
  const snapshot = await firebase.firestore().collection('characters').where('id', '==', characterId).get();
  const character = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
  return character[0];
};

export const getAllUsers = async () => {
  const snapshot = await firebase.firestore().collection('users').get();
  const users = snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }));
  return users;
};

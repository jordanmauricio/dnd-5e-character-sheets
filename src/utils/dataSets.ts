// TODO: format the data here
// {
//   transmutation: "T",
//   necromancy: "N",
//   conjuration: "C",
//   abjuration: "A",
//   enchantment: "E",
//   evocation: "V",
//   illusion: "I",
//   divination: "D"
// }
// TagCondition._CONDITIONS = ["blinded", "charmed", "deafened", "exhaustion", "frightened", "grappled", "incapacitated", "invisible", "paralyzed", "petrified", "poisoned", "prone", "restrained", "stunned", "unconscious"];

export const skills = [
  { label: 'Acrobatics', value: 'acrobatics', type: 'dex' },
  { label: 'Animal Handling', value: 'animal-handling', type: 'wis' },
  { label: 'Arcana', value: 'arcana', type: 'int' },
  { label: 'Atheltics', value: 'athletics', type: 'str' },
  { label: 'Deception', value: 'deception', type: 'cha' },
  { label: 'History', value: 'history', type: 'int' },
  { label: 'Insight', value: 'insight', type: 'wis' },
  { label: 'Intimidation', value: 'intimidation', type: 'cha' },
  { label: 'Investigation', value: 'investigation', type: 'int' },
  { label: 'Medicine', value: 'medicine', type: 'int' },
  { label: 'Nature', value: 'nature', type: 'int' },
  { label: 'Perception', value: 'perception', type: 'wis' },
  { label: 'Performance', value: 'performance', type: 'cha' },
  { label: 'Persuasion', value: 'persuasion', type: 'cha' },
  { label: 'Religion', value: 'religion', type: 'int' },
  { label: 'Sleight of Hand', value: 'sleight-of-hand', type: 'dex' },
  { label: 'Stealth', value: 'stealth', type: 'dex' },
  { label: 'Survival', value: 'survival', type: 'wis' },
];

export const abilities = [
  { label: 'Strength', value: 'str' },
  { label: 'Dexterity', value: 'dex' },
  { label: 'Constitution', value: 'con' },
  { label: 'Intelligence', value: 'int' },
  { label: 'Wisdom', value: 'wis' },
  { label: 'Charisma', value: 'cha' },
];

import cookie from 'cookie';
import { skills } from './dataSets';

export const levels = Array.from({ length: 20 }, (_, i) => ({
  value: i,
  label: `${i + 1}`,
}));

export const createRange = (spell: any) => {
  const type = spell.range.type;
  const distance = spell.range.distance ? ` | ${spell.range.distance.amount ?? ''} ${spell.range.distance.type}` : '';
  return `${type}${distance}`;
};

export const createTime = (spell: any) => {
  const firstTime = spell.time[0];
  return `${firstTime.number} ${firstTime.unit}`;
};

export const createDuration = (spell: any) => {
  const time = spell.duration.length > 0 ? spell.duration[0] : null;
  const duration = time.duration ? `${time.duration.amount} ${time.duration.type}` : '';
  const concentration = !!time.concentration;
  return { duration, concentration };
};

export const getProficiency = ({ value }: { value: number; label: string }) => {
  const level = value + 1;
  if (level >= 17) {
    return '6';
  } else if (level >= 13) {
    return '5';
  } else if (level >= 9) {
    return '4';
  } else if (level >= 5) {
    return '3';
  } else {
    return '2';
  }
};

export const getAbilityMods = ({ ability: abilities, class: dndClass, proficiency }: any) => {
  const saveProf = dndClass.saveProficiency;
  const updatedAbilities: any = {};

  for (const [ability, value] of Object.entries(abilities)) {
    const mod = Math.floor((Number(value as string) - 10) / 2);
    const save = saveProf[0] === ability || saveProf[1] === ability ? Number(mod + Number(proficiency)) : mod;

    updatedAbilities[ability] = {
      score: value,
      mod,
      save,
    };
  }
  return updatedAbilities;
};

export const getSpellMods = ({ class: dndClass, proficiency, ability }: any) => {
  if (!dndClass.castingAbility) return;
  const spellBonus = Number(proficiency) + ability[dndClass.castingAbility].mod;
  return {
    spellSave: 8 + spellBonus,
    spellAttack: spellBonus,
  };
};

export const getSkills = ({ skills: skillProficiencies, proficiency, ability }: any) => {
  const updatedSkills = [];

  for (const [skillKey, value] of Object.entries(skillProficiencies)) {
    const skillDetails = skills.filter((skill) => skill.value === skillKey);

    if (value === 'none') {
      const mod: number = ability[skillDetails[0].type].mod;
      updatedSkills.push({
        name: skillDetails[0].label,
        type: skillDetails[0].type,
        mod,
        passive: `${10 + mod}`,
        proficiency: value,
      });
    } else if (value === 'half') {
      const mod = Math.floor(ability[skillDetails[0].type].mod + Number(proficiency / 2));
      updatedSkills.push({
        name: skillDetails[0].label,
        type: skillDetails[0].type,
        mod,
        passive: `${10 + mod}`,
        proficiency: value,
      });
    } else if (value === 'full') {
      const mod = Math.floor(ability[skillDetails[0].type].mod + Number(proficiency));
      updatedSkills.push({
        name: skillDetails[0].label,
        type: skillDetails[0].type,
        mod,
        passive: `${10 + mod}`,
        proficiency: value,
      });
    } else if (value === 'expert') {
      const mod = Math.floor(ability[skillDetails[0].type].mod + Number(proficiency * 2));
      updatedSkills.push({
        name: skillDetails[0].label,
        type: skillDetails[0].type,
        mod,
        passive: `${10 + mod}`,
        proficiency: value,
      });
    }
  }
  return updatedSkills;
};

export const checkForSource = (entity: { source: string }) =>
  entity.source === 'PHB' ||
  entity.source === 'VGM' ||
  entity.source === 'MOT' ||
  entity.source === 'TCE' ||
  entity.source === 'MTF' ||
  entity.source === 'XGE';

export function parseCookies(req: any) {
  return cookie.parse(req ? req.headers.cookie || '' : document.cookie);
}

export function remapIsNew(item: any) {
  if (typeof item === 'object' && item.__isNew__) {
    delete item.__isNew__;
    return {
      ...item,
      isHomebrew: true,
    };
  } else {
    return item;
  }
}

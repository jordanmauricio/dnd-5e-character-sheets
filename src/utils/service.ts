import axios from 'axios';

export const addCharacterApi = async (auth: any, data: any) => {
  try {
    const header = {
      'Content-Type': 'application/json',
      token: auth.token,
    };
    const resp = await axios.post('/api/character', data, { headers: header });
    return resp;
  } catch (error) {
    throw error;
  }
};

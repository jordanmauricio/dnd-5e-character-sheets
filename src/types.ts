export type Spell = {
  value: string;
  label: string;
  from: string;
};

export type Source = {
  page: number;
  source: string;
};

export type SelectField = {
  label: string;
  value: string;
  from: string;
  isHomebrew?: boolean;
};

export type Feature =
  | string
  | {
      classFeature: string;
      gainSubclassFeature: boolean;
    };

export enum Ability {
  'str',
  'dex',
  'con',
  'int',
  'wis',
  'cha',
}

export type Character = {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  name: string;
  playerName: string;
  level: {
    value: number;
    label: string;
  };
  race: Race;
  subrace:
    | undefined
    | {
        label: string;
        value: string;
        name: string;
        page: number;
        hasFluff: boolean | undefined;
        hasFluffImages: boolean | undefined;
        ability: unknown[];
        entries: unknown[];
      };
  class: CharacterClass;
  subclass:
    | undefined
    | {
        label: string;
        name: string;
        value: string;
        fullName: string;
        from: string;
        features: {
          name: string;
          from: string;
          level: number;
        }[];
      };
  abilityScores: {
    str: {
      score: string;
      mod: number;
      save: number;
    };
    dex: {
      score: string;
      mod: number;
      save: number;
    };
    con: {
      score: string;
      mod: number;
      save: number;
    };
    int: {
      score: string;
      mod: number;
      save: number;
    };
    wis: {
      score: string;
      mod: number;
      save: number;
    };
    cha: {
      score: string;
      mod: number;
      save: number;
    };
  };
  background: SelectField;
  feat: SelectField[];
  languages: Omit<SelectField, 'from'>[];
  gold: string;
  initiative: string;
  speed: string;
  armorClass: string;
  maxHp: string;
  proficiency: string;
  skills: {
    name: string;
    type: string;
    mod: number;
    passive: string;
  }[];
  alignment: string;
  feats: string[];
  spells: {
    label: string;
    value: string;
    time: string;
    school: string;
    range: string;
    level: number;
    from: string;
    ritual: boolean;
    isHomebrew?: boolean;
    entries: string | string[];
    // TODO: add classes that have the spell
    components: {
      v: boolean;
      s: boolean;
      m:
        | boolean
        | string
        | {
            cost?: number;
            consumes?: boolean;
            text: string;
          };
    };
    duration: {
      duration: string;
      concentration: boolean;
    };
  }[];
  items: {
    label: string;
    value: string;
    rarity: string;
    isHomebrew?: boolean;
    isBase: boolean;
    from: string;
    attunement: boolean | string;
  }[];
};

export type Race = {
  label: string;
  value: string;
  from: string;
  creatureTypes: string[] | null;
  darkvision: number | null;
  resist: string[] | null;
  additionalSpells: unknown;
  size: string;
  speed: number;
  srd: boolean | null;
  traitTags: string[];
  subraces: {
    label: string;
    value: string;
    page: number;
    source: string; // TODO: add in fetch
    hasFluff: boolean | null;
    hasFluffImages: boolean | null;
    entries: unknown[];
    ability: unknown[];
  }[];
};

export type CharacterClass = {
  cantrips: number[] | null;
  casterProgression: null | 'full' | '1/2' | '1/3' | 'pact' | 'artificer';
  castingAbility: keyof Ability;
  from: string;
  hd: string;
  label: string;
  value: string;
  name:
    | 'Artificer'
    | 'Barbarian'
    | 'Bard'
    | 'Cleric'
    | 'Druid'
    | 'Fighter'
    | 'Monk'
    | 'Paladin'
    | 'Ranger'
    | 'Rogue'
    | 'Sorcerer'
    | 'Warlock'
    | 'Wizard';
  preparesSpells: boolean;
  saveProficiency: string[];
  spellAttack: number | undefined;
  spellSave: number | undefined;
  spellsKnown: number[] | null;
  srd: boolean | undefined;
  optionalFeatureProgression: unknown;
  features: {
    className: string;
    classSource: string;
    entries: unknown;
    level: number;
    name: string;
    page: number;
    source: string;
    srd: boolean;
    isClassFeatureVariant: boolean | undefined;
  }[];
  subclasses: {
    name: string;
    value: string;
    label: string;
    fullName: string;
    from: string;
    features: {
      name: string;
      from: string;
      level: number;
    };
  }[];
};

export type Class = {
  class: {
    name: string;
    source: string;
    classFeatures: Feature[];
    otherSources?: Source[];
    srd?: boolean;
    isReprinted?: boolean;
    isSidekick?: boolean;
    hd?: {
      number: number;
      faces: number;
    };
    proficiency?: string[];
    classTableGroups?: unknown;
    startingProficiencies?: {
      armor?: unknown[];
      tools?: unknown[];
      weapons?: unknown[];
      skills?: unknown[];
    };
    startingEquipment?: {
      additionalFromBackground: boolean;
      default: string[];
      defaultData: unknown;
      goldAlternative?: string;
    };
    requirements?: {
      [k: string]: number;
    }[];
    multiclassing?: {
      requirements: unknown;
      proficienciesGained?: {
        armor?: unknown[];
        tools?: unknown[];
        weapons?: unknown[];
        skills?: unknown[];
      };
    };
    // casterProgression?: "full" | "1/2" | "1/3" | "pact" | "artificer" | undefined,
    casterProgression?: string | undefined;
    preparedSpells?: string;
    spellcastingAbility?: string;
    cantripProgression?: number[];
    spellsKnownProgression?: number[];
    spellsKnownProgressionFixed?: number[];
    spellsKnownProgressionFixedAllowLowerLevel?: boolean;
    spellsKnownProgressionFixedByLevel?: {
      [k: number]: {
        [k: number]: number;
      };
    };
    // TODO: add ClassTableGroups Row for Spell Slots
    optionalfeatureProgression?: {
      featureType: string;
      name: string;
      progression: number[];
    }[];
    subclassTitle?: string;
    subclasses?: {
      name: string;
      shortName: string;
      source: string;
      subclassFeatures: string[];
      otherSources?: Source[];
      isReprinted?: boolean;
      srd?: boolean;
      page?: number;
      casterProgression?: string;
      spellcastingAbility?: string;
      cantripProgression?: number[];
      spellsKnownProgression?: number[];
      subclassTableGroups?: unknown[];
      additionalSpells?: unknown[];
      subclassSpells?: unknown[];
      subSubclassSpells?: unknown[];
      className?: string;
      classSource?: string;
    }[];
    page?: number;
    fluff?: unknown[];
    classSpells?: unknown;
    languageProficiencies?: unknown;
  }[];
  classFeature: {
    name: string;
    source: string;
    srd?: boolean;
    className: string;
    classSource: string;
    level: number;
    header?: number;
    page?: number;
    type?: string;
    entries: any[];
    otherSources?: Source[];
    isClassFeatureVariant?: boolean;
  }[];
  subclassFeature?: {
    name: string;
    source: string;
    srd?: boolean;
    className: string;
    classSource: string;
    subclassShortName: string;
    subclassSource: string;
    level: number;
    header?: number;
    page?: number;
    type?: string;
    entries: any[];
    isClassFeatureVariant?: boolean;
    otherSources?: Source[];
  }[];
};

export type FormattedClass = {
  name: string;
  label: string;
  value: string;
  from: string;
  hd: string;
  srd: boolean;
  features:
    | {
        name: string;
        source: string;
        srd?: boolean | undefined;
        className: string;
        classSource: string;
        level: number;
        header?: number | undefined;
        page?: number | undefined;
        type?: string | undefined;
        entries: any[] | string;
        otherSources?: Source[] | undefined;
        isClassFeatureVariant?: boolean | undefined;
      }[]
    | undefined;
  saveProficiency: string[] | undefined;
  casterProgression: string | null;
  castingAbility: string | null;
  cantrips: number[] | null;
  spellsKnown: number[] | null;
  preparesSpells: boolean;
  optionalFeatureProgression: null | unknown;
  subclasses:
    | (
        | {
            name: string;
            fullName: string;
            from: string;
            label: string;
            value: string;
            features: false | { name: string; from: string; level: number }[];
          }
        | undefined
      )[]
    | undefined;
};

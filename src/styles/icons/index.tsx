import { ReactElement } from 'react';

import { DndLogo } from './dndLogo';
import { Artificer } from './Artificer';
import { Barbarian } from './Barbarian';
import { Bard } from './Bard';
import { Cleric } from './Cleric';
import { Druid } from './Druid';
import { Fighter } from './Fighter';
import { Monk } from './Monk';
import { Paladin } from './Paladin';
import { Ranger } from './Ranger';
import { Rogue } from './Rogue';
import { Sorcerer } from './Sorcerer';
import { Warlock } from './Warlock';
import { Wizard } from './Wizard';

export type IconKey = keyof typeof icons;
export type IconColor = 'primary' | 'secondary' | 'white';

export const icons = {
  Artificer: Artificer,
  Barbarian: Barbarian,
  Bard: Bard,
  Cleric: Cleric,
  Druid: Druid,
  Fighter: Fighter,
  Monk: Monk,
  Paladin: Paladin,
  Ranger: Ranger,
  Rogue: Rogue,
  Sorcerer: Sorcerer,
  Warlock: Warlock,
  Wizard: Wizard,
  DndLogo: DndLogo,
};

export interface IconProps {
  icon: IconKey;
  color?: IconColor;
  height?: number;
  width?: number;
  rotate?: number;
  fill?: string;
  className?: string;
  style?: any;
}

export const Icon = ({ icon, height = 25, width = 25, fill, style }: IconProps): ReactElement | null => {
  const IconElement = icons[icon];

  if (!IconElement) return null;

  return <IconElement height={height} fill={fill} width={width} {...style} />;
};

import { Box, Grid, GridItem, Text, Button, ButtonGroup } from '@chakra-ui/react';
import { Character } from '../../types';

export const Modifiers = ({ abilityScores }: { abilityScores: Character['abilityScores'] }) => (
  <Grid templateColumns="repeat(3, 1fr)" templateRows="repeat(2, 1fr)" gap="1" p="0">
    <GridItem colSpan={1} colStart={1}>
      <Modifier name="Str" ability={abilityScores.str} />
    </GridItem>
    <GridItem colSpan={1} colStart={1}>
      <Modifier name="Int" ability={abilityScores.int} />
    </GridItem>
    <GridItem colSpan={1} colStart={2} rowStart={1}>
      <Modifier name="Dex" ability={abilityScores.dex} />
    </GridItem>
    <GridItem colSpan={1} colStart={2}>
      <Modifier name="Wis" ability={abilityScores.wis} />
    </GridItem>
    <GridItem colSpan={1} colStart={3} rowStart={1}>
      <Modifier name="Con" ability={abilityScores.con} />
    </GridItem>
    <GridItem colSpan={1} colStart={3}>
      <Modifier name="Cha" ability={abilityScores.cha} />
    </GridItem>
  </Grid>
);

export const Modifier = ({ name, ability }: { name: string; ability: any }) => {
  const { score, mod, save } = ability;
  return (
    <Box
      // borderRadius="md"
      // borderWidth="1px"
      // borderColor="green"
      minWidth="3.5em"
      minHeight="5.5em"
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
    >
      <Text fontSize="xl">{`${name}: ${score}`}</Text>
      {mod == save ? (
        <Button size="md" variant="outline" width="5.5em" borderColor="var(--chakra-colors-red-600)">
          <Text>+{mod}</Text>
        </Button>
      ) : (
        <ButtonGroup isAttached variant="outline" maxWidth="5.5em">
          <Button borderColor="var(--chakra-colors-red-600)">
            <Text>+{mod}</Text>
          </Button>
          <Button borderColor="var(--chakra-colors-red-600)">
            <Text>+{save}</Text>
          </Button>
        </ButtonGroup>
      )}
    </Box>
  );
};

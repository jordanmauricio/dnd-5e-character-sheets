import { Text, List, ListItem, Button, Grid, GridItem, As } from '@chakra-ui/react';
import { BsCircle, BsCircleHalf, BsCheck, BsCheckAll } from 'react-icons/bs';

import { Character } from '../../types';

export const Skills = ({ skills }: { skills: Character['skills'] }) => (
  <List pt={1.5}>
    {skills.map((skill) => (
      // TODO: set proficiency type icon
      // const profIcon = setProficiencyType(skill.proficiency);
      // <Text key={skill.name}>{`${skill.name} /${skill.mod}/ (${skill.passive}) ${skill.type}`}</Text>
      <ListItem key={skill.name} py={1}>
        <Grid templateColumns="20px auto 50px 50px 50px" alignItems="center">
          <GridItem>{/* <ListIcon as={profIcon} /> */}</GridItem>
          <GridItem>
            <Text>{skill.name}</Text>
          </GridItem>
          <GridItem>
            <Button size="sm" variant="outline" borderColor="var(--chakra-colors-red-600)">
              + {skill.mod}
            </Button>
          </GridItem>
          <GridItem justifySelf="center">
            <Text>{`(${skill.passive})`}</Text>
          </GridItem>
          <GridItem justifySelf="center">
            <Text textTransform="uppercase">{skill.type}</Text>
          </GridItem>
        </Grid>
      </ListItem>
    ))}
  </List>
);

export const setProficiencyType = (proficiency: string): As<any> => {
  switch (proficiency) {
    case 'expert':
      return BsCheckAll;
    case 'full':
      return BsCheck;
    case 'half':
      return BsCircleHalf;
    default:
      return BsCircle;
  }
};

import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Badge,
  Box,
  Button,
  Divider,
  Grid,
  Heading,
  Img,
  Text,
  GridItem,
} from '@chakra-ui/react';
import { BiShield, BiHeart } from 'react-icons/bi';
import { Character } from '../../types';

export const SheetHeader = ({ character }: { character: Character }) => (
  <Accordion allowToggle>
    <AccordionItem>
      <AccordionButton>
        <Grid templateRows="50% 50%" templateColumns="repeat(4, 1fr)">
          <GridItem colSpan={2} colStart={1}>
            <Heading size="md" textAlign="left">
              {character.name}
            </Heading>
          </GridItem>
          <GridItem colSpan={2} colStart={1}>
            <Text textAlign="left" fontSize="md">
              {`${character.race.label} | ${character.class.name} | ${character.level.label}`}
            </Text>
          </GridItem>
          <GridItem colStart={4}>
            <AccordionIcon />
          </GridItem>
        </Grid>
      </AccordionButton>
      <AccordionPanel pb={2}>
        <Grid templateColumns="repeat(3, 1fr)">
          <GridItem justifySelf="center">
            <Img
              borderRadius="full"
              boxSize="3em"
              src="https://www.naruto-guides.com/wp-content/uploads/2019/05/tsunade-senju.jpg"
            />
          </GridItem>
          <GridItem justifySelf="center">
            <Button size="lg" px={3} mx={0.5} variant="outline" colorScheme="blue" leftIcon={<BiShield />}>
              {character.armorClass}
            </Button>
          </GridItem>
          <GridItem justifySelf="center">
            <Button size="lg" px={3} mx={0.5} variant="outline" colorScheme="red" leftIcon={<BiHeart />}>
              {character.maxHp}
            </Button>
          </GridItem>
        </Grid>
        <Divider my="0.5em" orientation="horizontal" colorScheme="blackAlpha" />
        <Grid templateColumns="repeat(3, 1fr)">
          <GridItem justifySelf="center">
            <Box p="0.3em" borderRadius="md" borderWidth="1px" borderColor="green" textAlign="center" minWidth="3.5em">
              <Badge variant="outline" colorScheme="green">
                PROF
              </Badge>
              <Text>+{character.proficiency}</Text>
            </Box>
          </GridItem>
          <GridItem justifySelf="center">
            <Box p="0.3em" borderRadius="md" borderWidth="1px" borderColor="purple" textAlign="center" minWidth="3.5em">
              <Badge variant="outline" colorScheme="purple">
                MOV
              </Badge>
              <Text>{character.speed} FT</Text>
            </Box>
          </GridItem>
          <GridItem justifySelf="center">
            <Box p="0.3em" borderRadius="md" borderWidth="1px" textAlign="center" minWidth="3.5em">
              <Badge variant="outline">INIT</Badge>
              <Text>{character.initiative}</Text>
            </Box>
          </GridItem>
        </Grid>
      </AccordionPanel>
    </AccordionItem>
  </Accordion>
);

import Link from 'next/link';
import { Button, Text, Stack } from '@chakra-ui/react';
import { Character } from '../types';
import { Icon } from '../styles/icons';

export const SingleCharacter = ({ character }: { character: Character }) => {
  const subRace = character.subrace?.name ? `(${character.subrace.name})` : '';
  const subClass = character.subclass?.name ? `(${character.subclass.name})` : '';
  return (
    <Link href={`/character/${character.id}`}>
      <Button
        leftIcon={<Icon icon={character.class.name} width={38} height={38} />}
        variant="outline"
        colorScheme="red"
        justifyContent="flex-start"
        w="100%"
        maxW={400}
        m={2}
        py={12}
      >
        <Stack spacing={2}>
          <Text textAlign="left" fontWeight="700" color="gray.800" fontSize="lg">
            {character.name}
          </Text>
          <Text textAlign="left" color="gray.400" fontSize="sm">
            {`Level ${character.level.label} | ${character.race.label} ${subRace}`}
          </Text>
          <Text textAlign="left" color="gray.400" fontSize="sm">
            {`${character.class.label} ${subClass}`}
          </Text>
        </Stack>
      </Button>
    </Link>
  );
};

import {
  FormControl,
  FormLabel,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  NumberIncrementStepper,
  NumberDecrementStepper,
} from '@chakra-ui/react';
import { Controller } from 'react-hook-form';

export const AbilityStat = ({ id, label, control }: { id: string; label: string; control: any }) => {
  if (!control) return <></>;
  return (
    <Controller
      name={id}
      control={control}
      defaultValue="10"
      render={({ field }) => (
        <FormControl id={id}>
          <FormLabel>{label}</FormLabel>
          <NumberInput size="lg" w={24} min={1} max={24}>
            <NumberInputField maxW={32} {...field} />
            <NumberInputStepper>
              <NumberIncrementStepper />
              <NumberDecrementStepper />
            </NumberInputStepper>
          </NumberInput>
        </FormControl>
      )}
    />
  );
};

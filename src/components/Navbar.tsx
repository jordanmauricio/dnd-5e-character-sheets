import { Container, Box, Divider, Flex, Heading, Link } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react';
import { useAuth } from '../lib/auth';
import { IoMdPersonAdd } from 'react-icons/io';

export const Navbar = () => {
  const { auth, signOut } = useAuth();
  const router = useRouter();

  return (
    <Container w="100%" maxW="100%" p="0" bg="gray.800">
      <Flex justify="space-between" px={4} py={2} maxW={960} mx="auto">
        <Heading color="white" fontSize="lg" onClick={() => router.push('/')} as="button">
          Character Sheet 5e
        </Heading>
        <Box>
          {auth ? (
            <Flex p={2} alignItems="center">
              <Link
                py={1}
                px={4}
                color="white"
                fontWeight={router.pathname === '/create' ? 'extrabold' : 'normal'}
                onClick={() => router.push('/create')}
              >
                <IoMdPersonAdd display="inline-box" size="1.25em" />
              </Link>
              <Link color="white" p={2} onClick={() => signOut()}>
                Logout
              </Link>
            </Flex>
          ) : (
            <Box p={2}>
              <Link
                p={2}
                color="white"
                onClick={() => router.push('/signin')}
                fontWeight={router.pathname === '/signin' ? 'extrabold' : 'normal'}
              >
                Sign In
              </Link>
            </Box>
          )}
        </Box>
      </Flex>
      <Divider
        css={{
          boxShadow: '1px 1px #888888',
        }}
      />
    </Container>
  );
};

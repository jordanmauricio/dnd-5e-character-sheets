import { Input, FormControl, FormLabel, Text } from '@chakra-ui/react';
import { Controller, RegisterOptions, Control } from 'react-hook-form';

export const LabelInput = ({
  id,
  label,
  disabled = false,
  control,
  rules,
  errors,
}: {
  id: string;
  label: string;
  disabled?: boolean;
  errors?: any;
  control?: Control<any>; // FieldValues
  rules?: Omit<RegisterOptions, 'valueAsNumber' | 'valueAsDate' | 'setValueAs'>;
}) => {
  if (!control) return <></>;
  return (
    <Controller
      name={id}
      control={control}
      defaultValue=""
      rules={rules}
      render={({ field }) => (
        <FormControl id={id}>
          <FormLabel>{label}</FormLabel>
          <Input isInvalid={errors && errors[id]} type="text" disabled={disabled} {...field} />
          {errors && errors[id] && <Text color="red">{errors[id].message}</Text>}
        </FormControl>
      )}
    />
  );
};

import { useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Container, Heading, Button, Text } from '@chakra-ui/react';
import { Navbar, SingleCharacter } from '../src/components';
import { parseCookies } from '../src/utils/helpers';
import { getAllCharacters } from '../src/utils/db';
import { Character } from '../src/types';
import { useAuth } from '../src/lib/auth';

export default function Home({ characters }: { characters: Character[] }) {
  const { auth } = useAuth();
  const router = useRouter();

  useEffect(() => {
    if (!auth) {
      router.push('/signin');
    }
  }, [auth, router]);

  return (
    <div>
      <Head>
        <title>Character Creator</title>
        <meta name="description" content="Create a character for DnD 5e" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Navbar />
        <Container maxW="4xl" centerContent mb={28}>
          <Heading as="h3" size="lg" mt="4" mb="4">
            List of Characters
          </Heading>
          {characters.length === 0 && <Text>You don&apos;t seem to have any characters yet.</Text>}
          {characters.map((character) => (
            <SingleCharacter key={character.id} character={character} />
          ))}
          <Link href="/create">
            <Button mt={4} variant="solid" colorScheme="red" size="lg">
              Create a character
            </Button>
          </Link>
        </Container>
      </main>
    </div>
  );
}

export async function getServerSideProps(_context: any) {
  const cookies = parseCookies(_context.req);
  const characters = await getAllCharacters(cookies.userId);

  return { props: { characters } };
}

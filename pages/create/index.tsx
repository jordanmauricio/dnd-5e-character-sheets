import { useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import Select from 'react-select';
import { useForm, Controller, SubmitHandler } from 'react-hook-form';
// eslint-disable-next-line import/no-named-as-default
import Creatable from 'react-select/creatable';
import {
  FormControl,
  FormLabel,
  Flex,
  Container,
  Heading,
  SimpleGrid,
  Button,
  Wrap,
  WrapItem,
  Select as SimpleSelect,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Box,
  Text,
} from '@chakra-ui/react';
import { v4 as uuidv4 } from 'uuid';

import {
  spells,
  items,
  races,
  classes,
  backgrounds,
  feats,
  languages,
  invocations,
  elementalDiscipline,
  infusions,
  arcaneShot,
  metaMagic,
  runeKnightRunes,
  pactBoons,
  maneuvers,
  fightingStyles,
} from '../../src/data';

import { Character, Race, CharacterClass } from '../../src/types';
import { AbilityStat, LabelInput, Navbar } from '../../src/components';
import { levels, skills, getProficiency, getAbilityMods, getSpellMods, getSkills, remapIsNew } from '../../src/utils';
import { useAuth } from '../../src/lib/auth';
import { addCharacterApi } from '../../src/utils/service';

type CharacterCreateFormValues = Omit<
  Character,
  'id' | 'createdAt' | 'updatedAt' | 'class.spellSave' | 'class.spellAttack'
>;

export default function Create() {
  const { auth, loading } = useAuth();
  const router = useRouter();
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors, isSubmitting },
  } = useForm({ mode: 'onChange' });

  useEffect(() => {
    if (!auth && !loading) {
      router.push('/signin?next=/create');
    }
  }, [auth, loading, router]);

  const watchedClass = watch('class');
  const watchedRace = watch('race');

  const onSubmit: SubmitHandler<CharacterCreateFormValues> = async (data) => {
    const background = data.background ? remapIsNew(data.background) : data.background;
    data.background = background;

    const items = data.items?.length > 0 ? data.items.map((item: any) => remapIsNew(item)) : data.items;
    data.items = items;

    const spells = data.spells?.length > 0 ? data.spells.map((item: any) => remapIsNew(item)) : data.spells;
    data.spells = spells;

    const languages = data.languages?.length > 0 ? data.languages.map((item: any) => remapIsNew(item)) : data.languages;
    data.spells = languages;

    const proficiency = getProficiency(data.level);
    data.proficiency = proficiency;

    const updatedAbilities = getAbilityMods(data);
    data.abilityScores = updatedAbilities;

    const updatedSkills = getSkills(data);
    data.skills = updatedSkills;

    const spellMods = getSpellMods(data);
    data.class.spellAttack = spellMods?.spellAttack;
    data.class.spellSave = spellMods?.spellSave;

    const characterId = uuidv4();

    const character: Character = {
      id: characterId,
      createdAt: new Date(),
      updatedAt: new Date(),
      ...data,
    };

    try {
      console.log({ character });

      await addCharacterApi(auth, character);
      console.log('character has been added, waiting on push');
      router.push(`/character/${characterId}`);
    } catch (error) {
      console.error('Character Creation Error', error);
    }
  };

  const getSubclasses = (watchedClass: CharacterClass) => {
    const selectedClass = classes.filter((dndClass) => dndClass.value === watchedClass.value);
    return selectedClass[0].subclasses;
  };

  const getSubRaces = (watchedRace: Race) => {
    const selectedRace = races.filter((race) => race?.value === watchedRace.value);
    if (selectedRace.length > 0 && selectedRace[0]?.subraces) {
      return selectedRace[0].subraces;
    }
    return [];
  };

  return (
    <div>
      <Head>
        <title>Character Creator</title>
        <meta name="description" content="Create a character for DnD 5e" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Navbar />
        <Container maxW="960" centerContent mb={28}>
          <Heading as="h1" size="3xl" mt={16}>
            Character Creator
          </Heading>
          <Link href="/">Back to Home</Link>

          <form onSubmit={handleSubmit(onSubmit)}>
            <Wrap justify="space-between" my={4}>
              <WrapItem w={['100%', '32%', '32%']} minW="200">
                <LabelInput
                  id="name"
                  label="Name"
                  control={control}
                  errors={errors}
                  rules={{ required: 'This field is required' }}
                />
              </WrapItem>
              <WrapItem w={['100%', '32%', '32%']} minW="200">
                <LabelInput
                  id="playerName"
                  label="Player Name"
                  control={control}
                  errors={errors}
                  rules={{ required: 'This field is required' }}
                />
              </WrapItem>
              <WrapItem w={['100%', '32%', '32%']} minW="200">
                <Controller
                  name="level"
                  control={control}
                  rules={{ required: 'This fiels is required' }}
                  render={({ field }) => (
                    <FormControl id="level">
                      <FormLabel>Level</FormLabel>
                      <Select {...field} instanceId="level" options={levels} />
                      {errors.level && <Text color="red">{errors.level.message}</Text>}
                    </FormControl>
                  )}
                />
              </WrapItem>
            </Wrap>

            <Wrap my={4} justify="space-between">
              <WrapItem w={['100%', '32%', '32%']} minW="200">
                <Controller
                  name="race"
                  control={control}
                  rules={{ required: 'This field is required' }}
                  render={({ field }) => (
                    <FormControl id="race">
                      <FormLabel>Race</FormLabel>
                      <Select {...field} instanceId="race" options={races} />
                      {errors.race && <Text color="red">{errors.race.message}</Text>}
                    </FormControl>
                  )}
                />
              </WrapItem>
              <WrapItem w={['100%', '32%', '32%']} minW="200">
                <Controller
                  name="subrace"
                  control={control}
                  render={({ field }) => (
                    <FormControl id="subrace">
                      <FormLabel>Subrace</FormLabel>
                      <Select
                        {...field}
                        instanceId="subrace"
                        disabled={!!!watchedRace}
                        options={watchedRace?.value ? getSubRaces(watchedRace) : []}
                      />
                    </FormControl>
                  )}
                />
              </WrapItem>
              <WrapItem w={['100%', '32%', '32%']} minW="200">
                <Controller
                  name="background"
                  control={control}
                  render={({ field }) => (
                    <FormControl id="background">
                      <FormLabel>Background</FormLabel>
                      <Creatable {...field} instanceId="background" options={backgrounds} />
                    </FormControl>
                  )}
                />
              </WrapItem>
            </Wrap>

            <Wrap my={4} justify="space-between">
              <WrapItem w={['100%', '100%', '49%']} minW="200">
                <Controller
                  name="class"
                  control={control}
                  rules={{ required: 'This fiels is required' }}
                  render={({ field }) => (
                    <FormControl id="class">
                      <FormLabel>Class</FormLabel>
                      <Select {...field} instanceId="classSelector" options={classes} />
                      {errors.class && <Text color="red">{errors.class.message}</Text>}
                    </FormControl>
                  )}
                />
              </WrapItem>
              <WrapItem w={['100%', '100%', '49%']} minW="200">
                <Controller
                  name="subclass"
                  control={control}
                  render={({ field }) => (
                    <FormControl id="subclass">
                      <FormLabel>Subclass</FormLabel>
                      <Select
                        {...field}
                        instanceId="subclass"
                        disabled={!!!watchedClass}
                        options={watchedClass?.value ? getSubclasses(watchedClass) : []}
                      />
                    </FormControl>
                  )}
                />
              </WrapItem>
            </Wrap>

            <Wrap my={4} justify="space-between">
              <WrapItem>
                <AbilityStat id="ability.str" label="Strength" control={control} />
              </WrapItem>
              <WrapItem>
                <AbilityStat id="ability.dex" label="Dexterity" control={control} />
              </WrapItem>
              <WrapItem>
                <AbilityStat id="ability.con" label="Constitution" control={control} />
              </WrapItem>
              <WrapItem>
                <AbilityStat id="ability.int" label="Intelligence" control={control} />
              </WrapItem>
              <WrapItem>
                <AbilityStat id="ability.wis" label="Wisdom" control={control} />
              </WrapItem>
              <WrapItem>
                <AbilityStat id="ability.cha" label="Charisma" control={control} />
              </WrapItem>
            </Wrap>

            <Wrap my={4} justify="space-between">
              <WrapItem w={['100%', '48%', '24%']}>
                <LabelInput id="initiative" label="Initiative" control={control} />
              </WrapItem>
              <WrapItem w={['100%', '48%', '23%']}>
                <LabelInput id="armorClass" label="Armor Class" control={control} />
              </WrapItem>
              <WrapItem w={['100%', '48%', '24%']}>
                <LabelInput id="speed" label="Speed" control={control} />
              </WrapItem>
              <WrapItem w={['100%', '48%', '24%']}>
                <LabelInput id="alignment" label="Alignment" control={control} />
              </WrapItem>
            </Wrap>

            <Wrap my={4}>
              <WrapItem w={['100%', '48%', '24%']} minW="200">
                <Controller
                  name="languages"
                  control={control}
                  render={({ field }) => (
                    <FormControl id="languages">
                      <FormLabel>Languages</FormLabel>
                      <Creatable {...field} instanceId="languages" isMulti options={languages} />
                    </FormControl>
                  )}
                />
              </WrapItem>
              <WrapItem w={['100%', '48%', '24%']} minW="200">
                <Controller
                  name="feat"
                  control={control}
                  render={({ field }) => (
                    <FormControl id="feats">
                      <FormLabel>Feats</FormLabel>
                      <Select {...field} instanceId="feat" isMulti options={feats} />
                    </FormControl>
                  )}
                />
              </WrapItem>
              <WrapItem w={['100%', '48%', '24%']} minW="200">
                <LabelInput id="maxHp" label="Max HP" control={control} />
              </WrapItem>
              <WrapItem w={['100%', '48%', '24%']} minW="200">
                <LabelInput id="gold" label="Gold Amount" control={control} />
              </WrapItem>
            </Wrap>

            <Heading as="h3" size="md" my={4}>
              Skill Proficiencies
            </Heading>
            <SimpleGrid w="100%" minChildWidth="250px" spacing={1}>
              {skills.map((skill) => (
                <Controller
                  name={`skills[${skill.value}]`}
                  control={control}
                  defaultValue="none"
                  key={skill.value}
                  render={({ field }) => (
                    <Flex w="100%">
                      <FormControl key={skill.value} display="flex" alignItems="center">
                        <SimpleSelect {...field} size="xs" w="40%" maxW="85px" mr={2} mb={2}>
                          <option value="none">None</option>
                          <option value="half">Half</option>
                          <option value="full">Full</option>
                          <option value="expert">Double</option>
                        </SimpleSelect>
                        <FormLabel htmlFor={skill.value} mb={0}>
                          {`${skill.label} (${skill.type})`}
                        </FormLabel>
                      </FormControl>
                    </Flex>
                  )}
                />
              ))}
            </SimpleGrid>

            <Wrap my={4}>
              <WrapItem w={['100%', '100%', '49%']} minW="200">
                <Controller
                  name="spells"
                  control={control}
                  render={({ field }) => (
                    <FormControl id="spells">
                      <FormLabel>Spells</FormLabel>
                      <Creatable {...field} instanceId="spells" isMulti options={spells} />
                    </FormControl>
                  )}
                />
              </WrapItem>
              <WrapItem w={['100%', '100%', '49%']} minW="200">
                <Controller
                  name="items"
                  control={control}
                  render={({ field }) => (
                    <FormControl id="spells">
                      <FormLabel>Items</FormLabel>
                      <Creatable {...field} instanceId="items" isMulti options={items} />
                    </FormControl>
                  )}
                />
              </WrapItem>
            </Wrap>

            <Heading size="md" pt={8} pb={4}>
              Optional Features
            </Heading>
            <Accordion colorScheme="red" allowMultiple>
              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Arcane Shots
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.arcaneShots"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="arcaneShots">
                        <Creatable {...field} instanceId="arcaneShots" isMulti options={arcaneShot} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Elemental Discipline
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.elementalDiscipline"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="elementalDisciplines">
                        <Creatable {...field} instanceId="elementalDisciplines" isMulti options={elementalDiscipline} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Fighting Styles
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.fightingStyles"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="fightingStyles">
                        <Creatable {...field} instanceId="fightingStyles" isMulti options={fightingStyles} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Infusions
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.infusions"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="infusions">
                        <Creatable {...field} instanceId="infusions" isMulti options={infusions} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Invocations
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.invocations"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="invocations">
                        <Creatable {...field} instanceId="invocations" isMulti options={invocations} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Maneuvers
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.maneuvers"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="maneuvers">
                        <Creatable {...field} instanceId="maneuvers" isMulti options={maneuvers} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Metamagic
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.metaMagic"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="metaMagic">
                        <Creatable {...field} instanceId="metaMagic" isMulti options={metaMagic} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Pact Boons
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.pactBoons"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="pactBoons">
                        <Creatable {...field} instanceId="pactBoons" isMulti options={pactBoons} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <h2>
                  <AccordionButton>
                    <Box flex="1" textAlign="left">
                      Runes
                    </Box>
                    <AccordionIcon />
                  </AccordionButton>
                </h2>
                <AccordionPanel pb={4}>
                  <Controller
                    name="optionalFeatures.runes"
                    control={control}
                    render={({ field }) => (
                      <FormControl id="runes">
                        <Creatable {...field} instanceId="runes" isMulti options={runeKnightRunes} />
                      </FormControl>
                    )}
                  />
                </AccordionPanel>
              </AccordionItem>
            </Accordion>
            <Flex justifyContent="center">
              <Button type="submit" colorScheme="red" size="lg" my={4} isLoading={isSubmitting}>
                Create Character
              </Button>
            </Flex>
          </form>
        </Container>
      </main>
    </div>
  );
}

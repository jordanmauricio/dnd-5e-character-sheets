import { Button, Center, Container, Heading, VStack, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React from 'react';
import { FcGoogle } from 'react-icons/fc';
import { FaApple, FaGithub, FaTwitter } from 'react-icons/fa';
import { Navbar } from '../src/components';
import { useAuth } from '../src/lib/auth';

const Signin = () => {
  const { auth, siginWithGoogle } = useAuth();
  const router = useRouter();

  if (auth) {
    router.push((router.query.next as string) || '/');
  }

  return (
    <>
      <Navbar />
      <Container mb={28}>
        <Center mt={10}>
          <VStack spacing="4">
            <Heading fontSize="3xl" mb={2} textAlign="center">
              Please sign in to continue
            </Heading>
            <Button leftIcon={<FcGoogle />} onClick={() => siginWithGoogle()}>
              Sign In with Google
            </Button>
            <Text color="gray.400">Currently disabled, soon to be added:</Text>
            <Button disabled leftIcon={<FaApple />} onClick={() => siginWithGoogle()}>
              Sign In with Apple
            </Button>
            <Button disabled leftIcon={<FaGithub />} onClick={() => siginWithGoogle()}>
              Sign In with Github
            </Button>
            <Button disabled leftIcon={<FaTwitter fill="#1DA1F2" />} onClick={() => siginWithGoogle()}>
              Sign In with Twitter
            </Button>
          </VStack>
        </Center>
      </Container>
    </>
  );
};

export default Signin;

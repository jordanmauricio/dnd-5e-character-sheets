import { NextApiRequest, NextApiResponse } from 'next';
import { auth } from '../../src/lib/firebase-admin';
import { addCharacter as addCharacterFirebase } from '../../src/utils/db';

export default async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case 'POST':
      await addCharacter(req, res);
      break;
    default:
      res.status(405).json({ status: false, message: 'Method Not found' });
      break;
  }
};

const addCharacter = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const user = await auth.verifyIdToken(req.headers.token as string);
    const characterData = { uuid: user.uid, ...req.body };

    await addCharacterFirebase(characterData);
    return res.status(200).json({ status: true, message: 'Character added successfully...' });
  } catch (error) {
    console.error('Firebase Error on Character Create', error);
    return res.status(500).json({ status: false, message: 'Something went wrong', error });
  }
};

import type { AppProps } from 'next/app';
import { ThemeProvider, ThemeConfig, extendTheme, CSSReset } from '@chakra-ui/react';
import { AuthProvider } from '../src/lib/auth';
import { CookiesProvider } from 'react-cookie';

const config: ThemeConfig = {
  initialColorMode: 'light',
  useSystemColorMode: false,
};

export const theme = extendTheme({ config });

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <CSSReset />
      <AuthProvider>
        <CookiesProvider>
          <Component {...pageProps} />
        </CookiesProvider>
      </AuthProvider>
    </ThemeProvider>
  );
}
export default MyApp;

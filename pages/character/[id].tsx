import { useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { GetServerSideProps } from 'next';
import { Container, Tabs, TabList, Tab, TabPanels, TabPanel } from '@chakra-ui/react';
import { getCharacter } from '../../src/utils/db';
import { Character } from '../../src/types';
import { SheetHeader, Modifiers, Navbar, Skills } from '../../src/components';
import { GiFireSpellCast, GiPointySword, GiChest } from 'react-icons/gi';
import { IoPersonCircle } from 'react-icons/io5';
import { RiFilePaper2Line } from 'react-icons/ri';

type Props = {
  character: Character;
  error: string | null;
};

export default function CharacterSheet({ character, error }: Props) {
  const router = useRouter();

  useEffect(() => {
    if (error) {
      console.error(error);
      router.push('/');
    }
  }, [error, router]);

  return (
    <div>
      <Head>
        <title>Character Sheet</title>
        <meta name="description" content="Create a character for DnD 5e" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Navbar />
        <Container maxW="4xl" centerContent mb={28}>
          {error === null && <SheetHeader character={character} />}
          <Tabs defaultIndex={2} isFitted variant="enclosed-colored">
            <TabList>
              <Tab>
                <GiFireSpellCast size="2.1em" />
              </Tab>
              <Tab>
                <GiPointySword size="2.1em" />
              </Tab>
              <Tab>
                <IoPersonCircle size="2.1em" />
              </Tab>
              <Tab>
                <GiChest size="2.1em" />
              </Tab>
              <Tab>
                <RiFilePaper2Line size="2.1em" />
              </Tab>
            </TabList>

            <TabPanels>
              <TabPanel>
                <p>spells!</p>
              </TabPanel>
              <TabPanel>
                <p>attacks!</p>
              </TabPanel>
              <TabPanel>
                <Modifiers abilityScores={character.abilityScores} />
                <Skills skills={character.skills} />
              </TabPanel>
              <TabPanel>
                <p>inventory!</p>
              </TabPanel>
              <TabPanel>
                <p>extra!</p>
              </TabPanel>
            </TabPanels>
          </Tabs>
          <Link href="/">Back to Home</Link>
        </Container>
      </main>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps = async (_context) => {
  const { id } = _context.query;
  if (id && typeof id === 'string') {
    const character = await getCharacter(id);

    if (!character) return { props: { error: 'No character ID found' } };

    return { props: { character, error: null } };
  } else {
    return { props: { error: 'No character ID found' } };
  }
};

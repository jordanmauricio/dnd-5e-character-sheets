/* eslint-disable */

module.exports = {
  async redirects() {
    return [{
      source: '/character',
      destination: '/',
      permanent: true,
    }, ];
  },
};